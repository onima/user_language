require 'rspec'
require_relative '../services/githubcall'

describe GithubCall do
  let(:github_call) { GithubCall.new('onima') }

  describe 'user_favourite_language method' do
    it 'returns user favourite language' do
      expect_any_instance_of(GithubCall).
        to receive(:user_languages_count).
        and_return([["Ruby", 2], ["Python", 1], ["JavaScript", 3]])

      expect(github_call.user_favourite_language).to eq('JavaScript')
    end
  end

  describe 'user_languages_count method' do
    it 'returns an array with multiple arrays which contains languages and count for each of them' do
      expect_any_instance_of(GithubCall).
        to receive(:user_languages).
        and_return(
          ['Ruby', 'Python', 'JavaScript', 'Ruby', 'JavaScript', 'JavaScript']
      )

      expect(github_call.user_languages_count).
        to contain_exactly(["Ruby", 2], ["Python", 1], ["JavaScript", 3])
    end
  end
end
