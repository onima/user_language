require 'httparty'

class GithubCall

  def initialize(user_name)
    @user_name = user_name
    raise 'User not found' if github_user_exists? == false
  end

  def user_favourite_language
    languages_count = user_languages_count
    user_favourite_p_l = Hash[languages_count].sort_by { |_, v| v }.last #['language', integer]
    user_favourite_p_l.first #return language_name
  end

  def user_languages_count
    u_l = user_languages
    uniq_user_languages = u_l.uniq
    languages_keys = uniq_user_languages 

    languages_keys.map { |k| [k, u_l.count(k)] } #Ex: [["Ruby", 2], ["Python", 1], ["JavaScript", 3]]
  end

  private

  def user_languages
    user_languages_urls = user_repositories.map do |r| 
      r.fetch('languages_url')
    end
    user_languages_urls.flat_map do |url|
     languages = http_request(url).parsed_response
     languages.keys
    end
    # Ex: ['Ruby', 'Python', 'JavaScript', 'Ruby', 'JavaScript', 'JavaScript']
  end

  def user_repositories
    http_request("https://api.github.com/users/#{@user_name}/repos")
  end

  def http_request(url)
    HTTParty.get(url)
  end

  def github_user_exists?
    result = http_request("https://api.github.com/users/#{@user_name}")
    result['message'] == 'Not Found' ? false : true
  end
end
