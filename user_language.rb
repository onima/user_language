require 'sinatra'
require_relative './services/githubcall'

get '/' do
  erb :user_form
end

get '/search' do
  user_name = params[:username]
  
  begin
    githubcall = GithubCall.new(user_name)
  rescue RuntimeError => e
    status 404
    return e.message
  end

  githubcall.user_favourite_language
end

get '/horse' do
  erb :horse
end
