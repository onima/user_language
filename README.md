## User Language (code challenge)

### Ruby Version : 2.2.2

## To test this application locally

Run ``` bundle ``` ~> install Gems and dependencies

Run ``` ruby user_language.rb ``` ~> Launch server locally : localhost::4567

## To run tests

Running the test suite is simple : ``` rspec specs/githubcall_spec.rb ```

## To see HorseJS => localhost::4567/horse